<!----------------------------------------------------------------------------->

# compute (aws)

<!----------------------------------------------------------------------------->

## Description

Provides compute resources such as [EC2] virtual machines.

<!----------------------------------------------------------------------------->

## Modules

* [ebs/volume/attached](ebs/volume/attached/README.md) - Manage [EBS] volumes that are attached to [EC2] instances.
* [ebs/volume/detached](ebs/volume/detached/README.md) - Manage stand-alone [EBS] volumes.
* [ec2/instance/demand](ec2/instance/demand/README.md) - Manage [On-Demand](https://aws.amazon.com/ec2/pricing/on-demand) Elastic Compute Cloud ([EC2]) instances.
* [ec2/key](ec2/key/README.md) - Manage [EC2] SSH key-pairs.

<!----------------------------------------------------------------------------->

[EBS]: https://aws.amazon.com/ebs/
[EC2]: https://aws.amazon.com/ec2/

<!----------------------------------------------------------------------------->
