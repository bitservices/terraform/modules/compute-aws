################################################################################
# Modules
################################################################################

module "ebs_attached_volume" {
  source                 = "../ebs/volume/attached"
  name                   = format("swap-%s", module.ec2_demand_instance.name)
  size                   = 5
  zone                   = module.ec2_demand_instance.subnet_zone
  owner                  = local.owner
  company                = local.company
  attachment_instance_id = module.ec2_demand_instance.id
}

################################################################################
