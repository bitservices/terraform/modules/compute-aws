################################################################################
# Modules
################################################################################

module "ec2_demand_instance" {
  source      = "../ec2/instance/demand"
  key         = module.ec2_key.name
  vpc         = local.vpc
  class       = "foo.bar"
  owner       = local.owner
  company     = local.company
  subnet_zone = "b"
}

################################################################################
