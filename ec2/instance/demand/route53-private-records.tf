################################################################################
# Optional Variables
################################################################################

variable "route53_private_records_ttl" {
  type        = number
  default     = 300
  description = "The private records time-to-live in seconds."
}

################################################################################

variable "route53_private_records_ipv4_type" {
  type        = string
  default     = "A"
  description = "The private IPv4 record type. This should always be 'A'."
}

variable "route53_private_records_ipv4_create" {
  type        = bool
  default     = false
  description = "Should private IPv4 DNS records be created for this EC2 instance."
}

################################################################################

variable "route53_private_records_ipv6_type" {
  type        = string
  default     = "AAAA"
  description = "The private IPv6 record type. This should always be 'AAAA'."
}

variable "route53_private_records_ipv6_create" {
  type        = bool
  default     = true
  description = "Should private IPv6 DNS records be created for this EC2 instance."
}

################################################################################
# Locals
################################################################################

locals {
  route53_private_records_suffix = join("", data.aws_route53_zone.private.*.name)
}

################################################################################
# Resources
################################################################################

resource "aws_route53_record" "private_ipv4" {
  count   = var.route53_private_records_ipv4_create ? 1 : 0
  ttl     = var.route53_private_records_ttl
  name    = format("%s.%s", var.class, local.route53_private_records_suffix)
  type    = var.route53_private_records_ipv4_type
  records = tolist([aws_instance.scope.private_ip])
  zone_id = data.aws_route53_zone.private[0].id
}

################################################################################

resource "aws_route53_record" "private_ipv6" {
  count   = var.route53_private_records_ipv6_create ? 1 : 0
  ttl     = var.route53_private_records_ttl
  name    = format("%s.%s", var.class, local.route53_private_records_suffix)
  type    = var.route53_private_records_ipv6_type
  records = aws_instance.scope.ipv6_addresses
  zone_id = data.aws_route53_zone.private[0].id
}

################################################################################
# Outputs
################################################################################

output "route53_private_records_ipv4_create" {
  value = var.route53_private_records_ipv4_create
}

################################################################################

output "route53_private_records_ipv6_create" {
  value = var.route53_private_records_ipv6_create
}

################################################################################

output "route53_private_records_ipv4_ttl" {
  value = length(aws_route53_record.private_ipv4) == 1 ? aws_route53_record.private_ipv4[0].ttl : null
}

output "route53_private_records_ipv4_type" {
  value = length(aws_route53_record.private_ipv4) == 1 ? aws_route53_record.private_ipv4[0].type : null
}

output "route53_private_records_ipv4_name" {
  value = length(aws_route53_record.private_ipv4) == 1 ? aws_route53_record.private_ipv4[0].name : null
}

################################################################################

output "route53_private_records_ipv6_ttl" {
  value = length(aws_route53_record.private_ipv6) == 1 ? aws_route53_record.private_ipv6[0].ttl : null
}

output "route53_private_records_ipv6_type" {
  value = length(aws_route53_record.private_ipv6) == 1 ? aws_route53_record.private_ipv6[0].type : null
}

output "route53_private_records_ipv6_name" {
  value = length(aws_route53_record.private_ipv6) == 1 ? aws_route53_record.private_ipv6[0].name : null
}

################################################################################
