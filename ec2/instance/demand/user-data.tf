################################################################################
# Optional Variables
################################################################################

variable "user_data_extra_name" {
  type        = string
  default     = "99-extra-user-data"
  description = "A name to use for the extra user-data to pass to this EC2 instance."
}

variable "user_data_extra_content" {
  type        = string
  default     = null
  sensitive   = true
  description = "Extra user-data to pass to this EC2 instance. This should be a raw string and not a file path."
}

variable "user_data_extra_type" {
  type        = string
  default     = null
  description = "The MIME type of 'user_data_extra_content'. Defaults to cloud config."
}

################################################################################

variable "user_data_shell_type" {
  type        = string
  default     = "text/x-shellscript"
  description = "The MIME type for shell scripts. This setting should not be changed."
}

variable "user_data_cloudconfig_type" {
  type        = string
  default     = "text/cloud-config"
  description = "The MIME type for cloud config files. This setting should not be changed."
}

################################################################################
# Locals
################################################################################

locals {
  user_data_extra_content            = coalesce(var.user_data_extra_content, file(local.user_data_blank_cloudconfig_script))
  user_data_extra_type               = coalesce(var.user_data_extra_type, var.user_data_cloudconfig_type)
  user_data_core_script              = format("%s/user-data/00-core", path.module)
  user_data_platform_script          = format("%s/user-data/20-%s", path.module, var.platform)
  user_data_linux_swap_script        = format("%s/user-data/40-linux-swap.tpl", path.module)
  user_data_linux_data_script        = format("%s/user-data/60-linux-data.tpl", path.module)
  user_data_blank_shell_script       = format("%s/user-data/99-blank", path.module)
  user_data_blank_cloudconfig_script = format("%s/user-data/99-blank", path.module)
}

################################################################################
# Data Sources
################################################################################

data "template_file" "linux_swap_script" {
  template = file(local.user_data_linux_swap_script)

  vars = {
    "swap_disk" = var.volume_swap_device
  }
}

################################################################################

data "template_file" "linux_data_script" {
  template = file(local.user_data_linux_data_script)

  vars = {
    "data_disk"   = var.volume_data_device
    "data_mount"  = var.volume_data_mount
    "data_format" = local.volume_data_format
  }
}

################################################################################

data "template_cloudinit_config" "scope" {
  gzip          = true
  base64_encode = true

  part {
    filename     = basename(local.user_data_core_script)
    content_type = var.user_data_cloudconfig_type
    content      = file(local.user_data_core_script)
  }

  part {
    filename     = basename(local.user_data_platform_script)
    content_type = var.user_data_cloudconfig_type
    content      = file(local.user_data_platform_script)
  }

  part {
    filename     = basename(local.user_data_linux_swap_script)
    content_type = var.platform == "linux" ? var.user_data_shell_type : var.user_data_cloudconfig_type
    content      = var.platform == "linux" ? data.template_file.linux_swap_script.rendered : file(local.user_data_blank_cloudconfig_script)
  }

  part {
    filename     = basename(local.user_data_linux_data_script)
    content_type = var.platform == "linux" ? var.user_data_shell_type : var.user_data_cloudconfig_type
    content      = var.platform == "linux" ? data.template_file.linux_data_script.rendered : file(local.user_data_blank_cloudconfig_script)
  }

  part {
    filename     = var.user_data_extra_name
    content_type = local.user_data_extra_type
    content      = local.user_data_extra_content
  }
}

################################################################################
# Outputs
################################################################################

output "user_data_extra_name" {
  value = var.user_data_extra_name
}

output "user_data_extra_content" {
  sensitive = true
  value     = local.user_data_extra_content
}

output "user_data_extra_type" {
  value = local.user_data_extra_type
}

################################################################################

output "user_data_shell_type" {
  value = var.user_data_shell_type
}

output "user_data_cloudconfig_type" {
  value = var.user_data_cloudconfig_type
}

################################################################################

output "user_data_core_script" {
  value = local.user_data_core_script
}

output "user_data_platform_script" {
  value = local.user_data_platform_script
}

output "user_data_linux_swap_script" {
  value = local.user_data_linux_swap_script
}

output "user_data_linux_data_script" {
  value = local.user_data_linux_data_script
}

output "user_data_blank_shell_script" {
  value = local.user_data_blank_shell_script
}

output "user_data_blank_cloudconfig_script" {
  value = local.user_data_blank_cloudconfig_script
}

################################################################################

output "user_data_gzip_base64" {
  sensitive = true
  value     = data.template_cloudinit_config.scope.rendered
}

################################################################################
