<!----------------------------------------------------------------------------->

# ec2/instance/demand

#### Manage an [On-Demand](https://aws.amazon.com/ec2/pricing/on-demand) Elastic Compute Cloud ([EC2]) instance

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/compute/aws//ec2/instance/demand`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_ec2_key" {
  source = "gitlab.com/bitservices/compute/aws//ec2/key"
  name   = "my-key"
}

module "my_ec2_demand_instance" {
  source      = "gitlab.com/bitservices/compute/aws//ec2/instance/demand"
  key         =  module.my_ec2_key.name
  vpc         = "sandpit01"
  class       = "foo.bar"
  owner       = "terraform@bitservices.io"
  company     = "BITServices Ltd"
  subnet_zone = "b"
}
```

<!----------------------------------------------------------------------------->

[EC2]: https://aws.amazon.com/ec2

<!----------------------------------------------------------------------------->
