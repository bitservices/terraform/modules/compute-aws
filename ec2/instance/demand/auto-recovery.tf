################################################################################
# Optional Variables
################################################################################

variable "auto_recovery" {
  type        = bool
  default     = false
  description = "Automatically recover the instance if the instance is unexpectedly terminated."
}

variable "auto_recovery_period" {
  type        = number
  default     = 60
  description = "The period in seconds over which the instance availability is monitored."
}

variable "auto_recovery_threshold" {
  type        = number
  default     = 0
  description = "How many times the instance is allowed to be unavailable within 'auto_recovery_period', should always be '0'."
}

variable "auto_recovery_evaluation_periods" {
  type        = number
  default     = 3
  description = "The number of 'auto_recovery_period' over which instance availability is checked against 'auto_recovery_threshold'."
}

################################################################################
# Locals
################################################################################

locals {
  auto_recovery_alarm_actions = tolist([format("arn:aws:automate:%s:ec2:recover", data.aws_region.scope.name)])
}

################################################################################
# Resources
################################################################################

resource "aws_cloudwatch_metric_alarm" "scope" {
  count = var.auto_recovery ? 1 : 0

  namespace           = "AWS/EC2"
  statistic           = "Minimum"
  metric_name         = "StatusCheckFailed_System"
  alarm_description   = "EC2 Auto-Recovery Service"
  comparison_operator = "GreaterThanThreshold"

  period             = var.auto_recovery_period
  threshold          = var.auto_recovery_threshold
  alarm_name         = format("%s.%s", var.class, local.name_suffix)
  alarm_actions      = local.auto_recovery_alarm_actions
  evaluation_periods = var.auto_recovery_evaluation_periods

  dimensions = {
    "InstanceId" = aws_instance.scope.id
  }
}

################################################################################
# Outputs
################################################################################

output "auto_recovery" {
  value = var.auto_recovery
}

################################################################################

output "auto_recovery_id" {
  value = length(aws_cloudwatch_metric_alarm.scope) == 1 ? aws_cloudwatch_metric_alarm.scope[0].id : null
}

output "auto_recovery_period" {
  value = length(aws_cloudwatch_metric_alarm.scope) == 1 ? aws_cloudwatch_metric_alarm.scope[0].period : null
}

output "auto_recovery_namespace" {
  value = length(aws_cloudwatch_metric_alarm.scope) == 1 ? aws_cloudwatch_metric_alarm.scope[0].namespace : null
}

output "auto_recovery_statistic" {
  value = length(aws_cloudwatch_metric_alarm.scope) == 1 ? aws_cloudwatch_metric_alarm.scope[0].statistic : null
}

output "auto_recovery_threshold" {
  value = length(aws_cloudwatch_metric_alarm.scope) == 1 ? aws_cloudwatch_metric_alarm.scope[0].threshold : null
}

output "auto_recovery_dimensions" {
  value = length(aws_cloudwatch_metric_alarm.scope) == 1 ? aws_cloudwatch_metric_alarm.scope[0].dimensions : null
}

output "auto_recovery_alarm_name" {
  value = length(aws_cloudwatch_metric_alarm.scope) == 1 ? aws_cloudwatch_metric_alarm.scope[0].alarm_name : null
}

output "auto_recovery_metric_name" {
  value = length(aws_cloudwatch_metric_alarm.scope) == 1 ? aws_cloudwatch_metric_alarm.scope[0].metric_name : null
}

output "auto_recovery_alarm_actions" {
  value = length(aws_cloudwatch_metric_alarm.scope) == 1 ? aws_cloudwatch_metric_alarm.scope[0].alarm_actions : null
}

output "auto_recovery_alarm_description" {
  value = length(aws_cloudwatch_metric_alarm.scope) == 1 ? aws_cloudwatch_metric_alarm.scope[0].alarm_description : null
}

output "auto_recovery_evaluation_periods" {
  value = length(aws_cloudwatch_metric_alarm.scope) == 1 ? aws_cloudwatch_metric_alarm.scope[0].evaluation_periods : null
}

output "auto_recovery_comparison_operator" {
  value = length(aws_cloudwatch_metric_alarm.scope) == 1 ? aws_cloudwatch_metric_alarm.scope[0].comparison_operator : null
}


################################################################################
