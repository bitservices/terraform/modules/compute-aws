#!/bin/bash
################################################################################

SWAP_DISK="${swap_disk}"

################################################################################

if [ -n "$${SWAP_DISK}" ]; then
if [ "$${SWAP_DISK,,}" != "/dev/sda"   ] \
&& [ "$${SWAP_DISK,,}" != "/dev/sda1"  ] \
&& [ "$${SWAP_DISK,,}" != "/dev/xvda"  ] \
&& [ "$${SWAP_DISK,,}" != "/dev/xvda1" ]; then
    SWAP_TIMEOUT_COUNT=0

    while [ ! -b "$${SWAP_DISK}" ] && [ $${SWAP_TIMEOUT_COUNT} -lt 10 ]; do
      echo "Waiting for Swap Disk: $${SWAP_DISK}..."
      SWAP_TIMEOUT_COUNT=$(( SWAP_TIMEOUT_COUNT + 1 ))
      sleep 60
    done

    sleep 10

    SWAP_BLKID="$(blkid -s TYPE "$${SWAP_DISK}")"

    if [[ $${SWAP_BLKID,,} != *" type=\"swap\""* ]]; then
      mkswap --label "INSTANCE_SWAP" "$${SWAP_DISK}"
    fi

    SWAP_LINE="LABEL=INSTANCE_SWAP    none    swap    defaults      0   0"
    grep -i "$${SWAP_LINE}" "/etc/fstab"

    if [ $${?} -ne 0 ]; then
      echo "$${SWAP_LINE}" >> /etc/fstab
    fi

    swapon --all
fi
fi

################################################################################
