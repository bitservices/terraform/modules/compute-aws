#!/bin/bash
################################################################################

DATA_DISK="${data_disk}"
DATA_MOUNT="${data_mount}"
DATA_FORMAT="${data_format}"

################################################################################

if [ "$${DATA_FORMAT}" == "none" ]; then exit 0; fi

################################################################################

if [ -n "$${DATA_DISK}" ]; then
if [ "$${DATA_DISK,,}" != "/dev/sda"   ] \
&& [ "$${DATA_DISK,,}" != "/dev/sda1"  ] \
&& [ "$${DATA_DISK,,}" != "/dev/xvda"  ] \
&& [ "$${DATA_DISK,,}" != "/dev/xvda1" ]; then
    DATA_TIMEOUT_COUNT=0

    while [ ! -b "$${DATA_DISK}" ] && [ $${DATA_TIMEOUT_COUNT} -lt 10 ]; do
      echo "Waiting for Data Disk: $${DATA_DISK}..."
      DATA_TIMEOUT_COUNT=$(( DATA_TIMEOUT_COUNT + 1 ))
      sleep 60
    done

    sleep 10

    DATA_BLKID="$(blkid -s TYPE "$${DATA_DISK}")"

    if [[ $${DATA_BLKID,,} != *" type=\"$${DATA_FORMAT}\""* ]]; then
      if [ "$${DATA_FORMAT}" == "btrfs" ]; then
        mkfs.btrfs --force --label "INSTANCE_DATA" "$${DATA_DISK,,}"
      elif [ "$${DATA_FORMAT}" == "ext4" ]; then
        mkfs.ext4 -F -L "INSTANCE_DATA" -m 0 "$${DATA_DISK,,}"
      else
        echo "$${DATA_FORMAT} is NOT supported! Aborting!"
        exit 1
      fi
    fi

    if [ "$${DATA_FORMAT}" == "btrfs" ]; then
      DATA_LINE="LABEL=INSTANCE_DATA    $${DATA_MOUNT}    btrfs    rw,noatime,nodiratime,compress=lzo,space_cache=v2    0    0"
    elif [ "$${DATA_FORMAT}" == "ext4" ]; then
      DATA_LINE="LABEL=INSTANCE_DATA    $${DATA_MOUNT}    ext4    rw,noatime,nodiratime    0    0"
    else
      echo "$${DATA_FORMAT} is NOT supported! Aborting!"
      exit 1
    fi

    grep -i "$${DATA_LINE}" "/etc/fstab"

    if [ $${?} -ne 0 ]; then
      echo "$${DATA_LINE}" >> /etc/fstab
    fi

    mkdir -p "$${DATA_MOUNT}"
    mount --all
fi
fi

################################################################################
