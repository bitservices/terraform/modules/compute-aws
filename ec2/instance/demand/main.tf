################################################################################
# Required Variables
################################################################################

variable "key" {
  type        = string
  description = "The SSH key name to use for this instance."
}

variable "class" {
  type        = string
  description = "This forms the name of the EC2 instance. It is prefixed to the private DNS domain."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "type" {
  type        = string
  default     = "t3.nano"
  description = "The type of instance to create."
}

variable "tenancy" {
  type        = string
  default     = "default"
  description = "The tenancy of the instance."
}

variable "platform" {
  type        = string
  default     = "linux"
  description = "The operating system platform the instance will be running. This helps to generate the correct default user-data."
}

variable "iam_profile" {
  type        = string
  default     = null
  description = "The IAM Instance Profile to launch this instance with."
}

variable "t_unlimited" {
  type        = bool
  default     = false
  description = "Should t-series instances use unlimited credits (for an extra cost)."
}

variable "ebs_optimised" {
  type        = bool
  default     = false
  description = "If 'true', the launched EC2 instance will be EBS optimized."
}

variable "placement_group" {
  type        = string
  default     = null
  description = "The placement group of the instance."
}

variable "shutdown_behavior" {
  type        = string
  default     = "stop"
  description = "Instance initiated shutdown behavior for this instance."
}

variable "source_dest_check" {
  type        = bool
  default     = true
  description = "Controls if traffic is routed to the instance when the destination address does not match the instances address."
}

variable "detailed_monitoring" {
  type        = bool
  default     = false
  description = "If 'true', the launched EC2 instance will have detailed monitoring enabled."
}

variable "private_ipv4_index" {
  type        = number
  default     = 0
  description = "Offset to use to generate the static IPv4 address for this instance within the subnet that it will reside. '0' for DHCP."
}

variable "associate_public_ipv4" {
  type        = bool
  default     = false
  description = "Associate a public IPv4 address with the created instance."
}

variable "disable_api_termination" {
  type        = bool
  default     = false
  description = "If 'true', enables EC2 instance termination protection."
}

################################################################################

variable "ephemeral_1" {
  type        = bool
  default     = false
  description = "Should the 1st ephemeral device be included in the block device mapping."
}

variable "ephemeral_1_device_name" {
  type        = string
  default     = "/dev/sdd"
  description = "The name of the 1st ephemeral device to mount on the instance."
}

variable "ephemeral_1_virtual_name" {
  type        = string
  default     = "ephemeral0"
  description = "The instance store device name of the 1st ephemeral device."
}

################################################################################

variable "ephemeral_2" {
  type        = bool
  default     = false
  description = "Should the 2nd ephemeral device be included in the block device mapping."
}

variable "ephemeral_2_device_name" {
  type        = string
  default     = "/dev/sde"
  description = "The name of the 2nd ephemeral device to mount on the instance."
}

variable "ephemeral_2_virtual_name" {
  type        = string
  default     = "ephemeral1"
  description = "The instance store device name of the 2nd ephemeral device."
}

################################################################################

variable "ephemeral_3" {
  type        = bool
  default     = false
  description = "Should the 3rd ephemeral device be included in the block device mapping."
}

variable "ephemeral_3_device_name" {
  type        = string
  default     = "/dev/sdf"
  description = "The name of the 3rd ephemeral device to mount on the instance."
}

variable "ephemeral_3_virtual_name" {
  type        = string
  default     = "ephemeral2"
  description = "The instance store device name of the 3rd ephemeral device."
}

################################################################################

variable "ephemeral_4" {
  type        = bool
  default     = false
  description = "Should the 4th ephemeral device be included in the block device mapping."
}

variable "ephemeral_4_device_name" {
  type        = string
  default     = "/dev/sdg"
  description = "The name of the 4th ephemeral device to mount on the instance."
}

variable "ephemeral_4_virtual_name" {
  type        = string
  default     = "ephemeral3"
  description = "The instance store device name of the 4th ephemeral device."
}

################################################################################

variable "ephemeral_5" {
  type        = bool
  default     = false
  description = "Should the 5th ephemeral device be included in the block device mapping."
}

variable "ephemeral_5_device_name" {
  type        = string
  default     = "/dev/sdh"
  description = "The name of the 5th ephemeral device to mount on the instance."
}

variable "ephemeral_5_virtual_name" {
  type        = string
  default     = "ephemeral4"
  description = "The instance store device name of the 5th ephemeral device."
}

################################################################################

variable "ephemeral_6" {
  type        = bool
  default     = false
  description = "Should the 6th ephemeral device be included in the block device mapping."
}

variable "ephemeral_6_device_name" {
  type        = string
  default     = "/dev/sdi"
  description = "The name of the 6th ephemeral device to mount on the instance."
}

variable "ephemeral_6_virtual_name" {
  type        = string
  default     = "ephemeral5"
  description = "The instance store device name of the 6th ephemeral device."
}

################################################################################

variable "ephemeral_7" {
  type        = bool
  default     = false
  description = "Should the 7th ephemeral device be included in the block device mapping."
}

variable "ephemeral_7_device_name" {
  type        = string
  default     = "/dev/sdj"
  description = "The name of the 7th ephemeral device to mount on the instance."
}

variable "ephemeral_7_virtual_name" {
  type        = string
  default     = "ephemeral6"
  description = "The instance store device name of the 7th ephemeral device."
}

################################################################################

variable "ephemeral_8" {
  type        = bool
  default     = false
  description = "Should the 8th ephemeral device be included in the block device mapping."
}

variable "ephemeral_8_device_name" {
  type        = string
  default     = "/dev/sdk"
  description = "The name of the 8th ephemeral device to mount on the instance."
}

variable "ephemeral_8_virtual_name" {
  type        = string
  default     = "ephemeral7"
  description = "The instance store device name of the 8th ephemeral device."
}

################################################################################

variable "ephemeral_9" {
  type        = bool
  default     = false
  description = "Should the 9th ephemeral device be included in the block device mapping."
}

variable "ephemeral_9_device_name" {
  type        = string
  default     = "/dev/sdl"
  description = "The name of the 9th ephemeral device to mount on the instance."
}

variable "ephemeral_9_virtual_name" {
  type        = string
  default     = "ephemeral8"
  description = "The instance store device name of the 9th ephemeral device."
}

################################################################################

variable "ephemeral_10" {
  type        = bool
  default     = false
  description = "Should the 10th ephemeral device be included in the block device mapping."
}

variable "ephemeral_10_device_name" {
  type        = string
  default     = "/dev/sdm"
  description = "The name of the 10th ephemeral device to mount on the instance."
}

variable "ephemeral_10_virtual_name" {
  type        = string
  default     = "ephemeral9"
  description = "The instance store device name of the 10th ephemeral device."
}

################################################################################

variable "ipv6_index" {
  type        = number
  default     = 0
  description = "Offset to use to generate the static IPv6 address for this instance within the subnet that it will reside. '0' for dynamic."
}

variable "ipv6_static" {
  type        = bool
  default     = null
  description = "Should the IPv6 address assigned be locked even against manual changes to the ENI? Defaults to 'true' if 'ipv6_index' is set, otherwise 'false'."
}

################################################################################

variable "metadata_v2" {
  type        = bool
  default     = true
  description = "Whether or not the metadata service requires session tokens, also referred to as Instance Metadata Service Version 2 (IMDSv2)."
}

variable "metadata_ipv6" {
  type        = bool
  default     = true
  description = "Whether the IPv6 endpoint for the instance metadata service is enabled."
}

variable "metadata_tags" {
  type        = bool
  default     = false
  description = "Enables or disables access to instance tags from the instance metadata service."
}

################################################################################

variable "volume_root_iops" {
  type        = number
  default     = 0
  description = "The amount of provisioned IOPS for the root device. This is only valid if 'volume_root_type' is 'gp3', 'io2' or 'io1'."
}

variable "volume_root_size" {
  type        = number
  default     = 8
  description = "The size of the root volume in gigabytes."
}

variable "volume_root_type" {
  type        = string
  default     = "standard"
  description = "The type of root volume. Can be 'standard', 'gp3', 'gp2', 'io2', 'io1', 'st1' or 'sc1'."

  validation {
    condition     = contains(["standard", "gp3", "gp2", "io2", "io1", "st1", "sc1"], var.volume_root_type)
    error_message = "The root volume must be of type: 'standard', 'gp3', 'gp2', 'io2', 'io1', 'st1' or 'sc1'."
  }
}

variable "volume_root_preserve" {
  type        = bool
  default     = false
  description = "Whether the root volume should be preserved on instance termination."
}

variable "volume_root_throughput" {
  type        = number
  default     = null
  description = "Throughput to provision for the root volume in mebibytes per second (MiB/s). This is only valid if 'volume_root_type' is 'gp3'."
}

################################################################################
# Locals
################################################################################

locals {
  ipv6_static            = coalesce(var.ipv6_static, var.ipv6_index != 0)
  name_suffix            = replace(join("", data.aws_route53_zone.private.*.name), "/\\.$/", "")
  volume_root_iops       = contains(["gp3", "io2", "io1"], var.volume_root_type) ? max(var.volume_root_iops, var.volume_root_type == "gp3" ? 3000 : 100) : null
  volume_root_throughput = var.volume_root_type == "gp3" ? var.volume_root_throughput : null
}

################################################################################
# Resources
################################################################################

resource "aws_instance" "scope" {
  ami                                  = data.aws_ami.scope.id
  tenancy                              = var.tenancy
  key_name                             = var.key
  subnet_id                            = data.aws_subnet.scope.id
  monitoring                           = var.detailed_monitoring
  private_ip                           = var.private_ipv4_index != 0 ? cidrhost(data.aws_subnet.scope.cidr_block, var.private_ipv4_index) : null
  ebs_optimized                        = var.ebs_optimised
  instance_type                        = var.type
  ipv6_addresses                       = var.ipv6_index != 0 ? tolist([cidrhost(data.aws_subnet.scope.ipv6_cidr_block, var.ipv6_index)]) : null
  placement_group                      = var.placement_group
  user_data_base64                     = data.template_cloudinit_config.scope.rendered
  source_dest_check                    = var.source_dest_check
  enable_primary_ipv6                  = local.ipv6_static
  iam_instance_profile                 = var.iam_profile
  vpc_security_group_ids               = concat(var.security_group_ids, data.aws_security_group.scope.*.id)
  disable_api_termination              = var.disable_api_termination
  associate_public_ip_address          = var.associate_public_ipv4
  instance_initiated_shutdown_behavior = var.shutdown_behavior

  tags = {
    "VPC"      = var.vpc
    "Name"     = format("%s.%s", var.class, local.name_suffix)
    "Zone"     = replace(data.aws_subnet.scope.availability_zone, data.aws_region.scope.name, "")
    "Class"    = var.class
    "Owner"    = var.owner
    "Region"   = data.aws_region.scope.name
    "Company"  = var.company
    "Metadata" = format("Version %d", var.metadata_v2 ? 2 : 1)
    "Platform" = var.platform
  }

  volume_tags = {
    "VPC"      = var.vpc
    "Name"     = format("%s.%s", var.class, local.name_suffix)
    "Zone"     = replace(data.aws_subnet.scope.availability_zone, data.aws_region.scope.name, "")
    "Class"    = var.class
    "Owner"    = var.owner
    "Region"   = data.aws_region.scope.name
    "Company"  = var.company
    "Platform" = var.platform
  }

  credit_specification {
    cpu_credits = var.t_unlimited ? "unlimited" : "standard"
  }

  ephemeral_block_device {
    no_device    = var.ephemeral_1 ? false : true
    device_name  = var.ephemeral_1_device_name
    virtual_name = var.ephemeral_1_virtual_name
  }

  ephemeral_block_device {
    no_device    = var.ephemeral_2 ? false : true
    device_name  = var.ephemeral_2_device_name
    virtual_name = var.ephemeral_2_virtual_name
  }

  ephemeral_block_device {
    no_device    = var.ephemeral_3 ? false : true
    device_name  = var.ephemeral_3_device_name
    virtual_name = var.ephemeral_3_virtual_name
  }

  ephemeral_block_device {
    no_device    = var.ephemeral_4 ? false : true
    device_name  = var.ephemeral_4_device_name
    virtual_name = var.ephemeral_4_virtual_name
  }

  ephemeral_block_device {
    no_device    = var.ephemeral_5 ? false : true
    device_name  = var.ephemeral_5_device_name
    virtual_name = var.ephemeral_5_virtual_name
  }

  ephemeral_block_device {
    no_device    = var.ephemeral_6 ? false : true
    device_name  = var.ephemeral_6_device_name
    virtual_name = var.ephemeral_6_virtual_name
  }

  ephemeral_block_device {
    no_device    = var.ephemeral_7 ? false : true
    device_name  = var.ephemeral_7_device_name
    virtual_name = var.ephemeral_7_virtual_name
  }

  ephemeral_block_device {
    no_device    = var.ephemeral_8 ? false : true
    device_name  = var.ephemeral_8_device_name
    virtual_name = var.ephemeral_8_virtual_name
  }

  ephemeral_block_device {
    no_device    = var.ephemeral_9 ? false : true
    device_name  = var.ephemeral_9_device_name
    virtual_name = var.ephemeral_9_virtual_name
  }

  ephemeral_block_device {
    no_device    = var.ephemeral_10 ? false : true
    device_name  = var.ephemeral_10_device_name
    virtual_name = var.ephemeral_10_virtual_name
  }

  metadata_options {
    http_tokens            = var.metadata_v2 ? "required" : "optional"
    http_protocol_ipv6     = var.metadata_ipv6 ? "enabled" : "disabled"
    instance_metadata_tags = var.metadata_tags ? "enabled" : "disabled"
  }

  root_block_device {
    iops                  = local.volume_root_iops
    throughput            = local.volume_root_throughput
    volume_type           = var.volume_root_type
    volume_size           = var.volume_root_size
    delete_on_termination = var.volume_root_preserve ? false : true
  }
}

################################################################################
# Outputs
################################################################################

output "class" {
  value = var.class
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "type" {
  value = var.type
}

output "tenancy" {
  value = var.tenancy
}

output "platform" {
  value = var.platform
}

output "iam_profile" {
  value = var.iam_profile
}

output "t_unlimited" {
  value = var.t_unlimited
}

output "ebs_optimised" {
  value = var.ebs_optimised
}

output "shutdown_behavior" {
  value = var.shutdown_behavior
}

output "source_dest_check" {
  value = var.source_dest_check
}

output "detailed_monitoring" {
  value = var.detailed_monitoring
}

output "associate_public_ipv4" {
  value = var.associate_public_ipv4
}

output "disable_api_termination" {
  value = var.disable_api_termination
}

################################################################################

output "ephemeral_1" {
  value = var.ephemeral_1
}

output "ephemeral_1_device_name" {
  value = var.ephemeral_1_device_name
}

output "ephemeral_1_virtual_name" {
  value = var.ephemeral_1_virtual_name
}

################################################################################

output "ephemeral_2" {
  value = var.ephemeral_2
}

output "ephemeral_2_device_name" {
  value = var.ephemeral_2_device_name
}

output "ephemeral_2_virtual_name" {
  value = var.ephemeral_2_virtual_name
}

################################################################################

output "ephemeral_3" {
  value = var.ephemeral_3
}

output "ephemeral_3_device_name" {
  value = var.ephemeral_3_device_name
}

output "ephemeral_3_virtual_name" {
  value = var.ephemeral_3_virtual_name
}

################################################################################

output "ephemeral_4" {
  value = var.ephemeral_4
}

output "ephemeral_4_device_name" {
  value = var.ephemeral_4_device_name
}

output "ephemeral_4_virtual_name" {
  value = var.ephemeral_4_virtual_name
}

################################################################################

output "ephemeral_5" {
  value = var.ephemeral_5
}

output "ephemeral_5_device_name" {
  value = var.ephemeral_5_device_name
}

output "ephemeral_5_virtual_name" {
  value = var.ephemeral_5_virtual_name
}

################################################################################

output "ephemeral_6" {
  value = var.ephemeral_6
}

output "ephemeral_6_device_name" {
  value = var.ephemeral_6_device_name
}

output "ephemeral_6_virtual_name" {
  value = var.ephemeral_6_virtual_name
}

################################################################################

output "ephemeral_7" {
  value = var.ephemeral_7
}

output "ephemeral_7_device_name" {
  value = var.ephemeral_7_device_name
}

output "ephemeral_7_virtual_name" {
  value = var.ephemeral_7_virtual_name
}

################################################################################

output "ephemeral_8" {
  value = var.ephemeral_8
}

output "ephemeral_8_device_name" {
  value = var.ephemeral_8_device_name
}

output "ephemeral_8_virtual_name" {
  value = var.ephemeral_8_virtual_name
}

################################################################################

output "ephemeral_9" {
  value = var.ephemeral_9
}

output "ephemeral_9_device_name" {
  value = var.ephemeral_9_device_name
}

output "ephemeral_9_virtual_name" {
  value = var.ephemeral_9_virtual_name
}

################################################################################

output "ephemeral_10" {
  value = var.ephemeral_10
}

output "ephemeral_10_device_name" {
  value = var.ephemeral_10_device_name
}

output "ephemeral_10_virtual_name" {
  value = var.ephemeral_10_virtual_name
}

################################################################################

output "metadata_v2" {
  value = var.metadata_v2
}

output "metadata_ipv6" {
  value = var.metadata_ipv6
}

output "metadata_tags" {
  value = var.metadata_tags
}

################################################################################

output "volume_root_iops" {
  value = local.volume_root_iops
}

output "volume_root_size" {
  value = var.volume_root_size
}

output "volume_root_type" {
  value = var.volume_root_type
}

output "volume_root_preserve" {
  value = var.volume_root_preserve
}

output "volume_root_throughput" {
  value = local.volume_root_throughput
}

################################################################################

output "id" {
  value = aws_instance.scope.id
}

output "key" {
  value = aws_instance.scope.key_name
}

output "name" {
  value = aws_instance.scope.tags.Name
}

output "ipv6_static" {
  value = aws_instance.scope.enable_primary_ipv6
}

output "ipv6_addresses" {
  value = aws_instance.scope.ipv6_addresses
}

output "placement_group" {
  value = aws_instance.scope.placement_group
}

output "availability_zone" {
  value = aws_instance.scope.availability_zone
}

output "public_dns_record" {
  value = aws_instance.scope.public_dns
}

output "private_dns_record" {
  value = aws_instance.scope.private_dns
}

output "public_ipv4_address" {
  value = aws_instance.scope.public_ip
}

output "private_ipv4_address" {
  value = aws_instance.scope.private_ip
}

################################################################################
