################################################################################
# Optional Variables
################################################################################

variable "volume_data_iops" {
  type        = number
  default     = 0
  description = "The amount of provisioned IOPS for the data device. This is only valid if 'data_device_type' is 'gp3', 'io2' or 'io1'."
}

variable "volume_data_size" {
  type        = number
  default     = 1
  description = "The size of the data volume in gigabytes."
}

variable "volume_data_type" {
  type        = string
  default     = "standard"
  description = "The type of data volume. Can be 'standard', 'gp3', 'gp2', 'io2', 'io1', 'st1' or 'sc1'."

  validation {
    condition     = contains(["standard", "gp3", "gp2", "io2", "io1", "st1", "sc1"], var.volume_data_type)
    error_message = "The data volume must be of type: 'standard', 'gp3', 'gp2', 'io2', 'io1', 'st1' or 'sc1'."
  }
}

variable "volume_data_mount" {
  type        = string
  default     = "/data"
  description = "Where should the data volume be mounted within the filesystem."
}

variable "volume_data_device" {
  type        = string
  default     = "/dev/xvdc"
  description = "The name of the data device to mount."
}

variable "volume_data_format" {
  type        = string
  default     = "none"
  description = "What format should the data device be. Can be 'btrfs', 'ext4' or 'none' to skip creation of a data volume."
}

variable "volume_data_encrypted" {
  type        = bool
  default     = true
  description = "Enables EBS encryption on the data volume."
}

variable "volume_data_throughput" {
  type        = number
  default     = null
  description = "Throughput to provision for the data volume in mebibytes per second (MiB/s). This is only valid if 'volume_data_type' is 'gp3'."
}

variable "volume_data_force_detach" {
  type        = bool
  default     = false
  description = "Set to 'true' if you want to force the data volume to detach. Useful if previous attempts failed, but use this option only as a last resort, as this can result in data loss."
}

variable "volume_data_skip_destroy" {
  type        = bool
  default     = false
  description = "Set to 'true' if you do not wish to detach the data volume from the instance to which it is attached at destroy time, and instead just remove the attachment from Terraform state."
}

################################################################################
# Locals
################################################################################

locals {
  volume_data_iops       = contains(["gp3", "io2", "io1"], var.volume_data_type) ? max(var.volume_data_iops, var.volume_data_type == "gp3" ? 3000 : 100) : null
  volume_data_format     = lower(trimspace(var.volume_data_format))
  volume_data_throughput = var.volume_data_type == "gp3" ? var.volume_data_throughput : null
}

################################################################################
# Resources
################################################################################

resource "aws_ebs_volume" "data" {
  count             = local.volume_data_format != "none" ? 1 : 0
  iops              = local.volume_data_iops
  size              = var.volume_data_size
  type              = var.volume_data_type
  encrypted         = var.volume_data_encrypted
  throughput        = local.volume_data_throughput
  availability_zone = data.aws_subnet.scope.availability_zone

  tags = {
    VPC      = var.vpc
    Name     = format("%s.%s", var.class, local.name_suffix)
    Zone     = replace(data.aws_subnet.scope.availability_zone, data.aws_region.scope.name, "")
    Class    = var.class
    Owner    = var.owner
    Region   = data.aws_region.scope.name
    Company  = var.company
    Platform = var.platform
  }
}

################################################################################

resource "aws_volume_attachment" "data" {
  count        = local.volume_data_format != "none" ? 1 : 0
  volume_id    = aws_ebs_volume.data[count.index].id
  device_name  = var.volume_data_device
  instance_id  = aws_instance.scope.id
  force_detach = var.volume_data_force_detach
  skip_destroy = var.volume_data_skip_destroy
}

################################################################################
# Outputs
################################################################################

output "volume_data_iops" {
  value = local.volume_data_iops
}

output "volume_data_size" {
  value = var.volume_data_size
}

output "volume_data_type" {
  value = var.volume_data_type
}

output "volume_data_mount" {
  value = var.volume_data_mount
}

output "volume_data_device" {
  value = var.volume_data_device
}

output "volume_data_format" {
  value = local.volume_data_format
}

output "volume_data_encrypted" {
  value = var.volume_data_encrypted
}

output "volume_data_throughput" {
  value = local.volume_data_throughput
}

output "volume_data_force_detach" {
  value = var.volume_data_force_detach
}

output "volume_data_skip_destroy" {
  value = var.volume_data_skip_destroy
}

################################################################################

output "volume_data_id" {
  value = length(aws_ebs_volume.data) == 1 ? aws_ebs_volume.data[0].id : null
}

output "volume_data_name" {
  value = length(aws_ebs_volume.data) == 1 ? aws_ebs_volume.data[0].tags.Name : null
}

################################################################################

output "volume_data_attachment_id" {
  value = length(aws_volume_attachment.data) == 1 ? aws_volume_attachment.data[0].id : null
}

################################################################################
