################################################################################
# Optional Variables
################################################################################

variable "volume_swap_iops" {
  type        = number
  default     = 0
  description = "The amount of provisioned IOPS for the swap device. This is only valid if 'swap_device_type' is 'gp3', 'io2' or 'io1'."
}

variable "volume_swap_size" {
  type        = number
  default     = 1
  description = "The size of the swap volume in gigabytes."
}

variable "volume_swap_type" {
  type        = string
  default     = "standard"
  description = "The type of swap volume. Can be 'standard', 'gp3', 'gp2', 'io2', 'io1', 'st1' or 'sc1'."

  validation {
    condition     = contains(["standard", "gp3", "gp2", "io2", "io1", "st1", "sc1"], var.volume_swap_type)
    error_message = "The swap volume must be of type: 'standard', 'gp3', 'gp2', 'io2', 'io1', 'st1' or 'sc1'."
  }
}

variable "volume_swap_device" {
  type        = string
  default     = "/dev/xvdb"
  description = "The name of the swap device to mount."
}

variable "volume_swap_encrypted" {
  type        = bool
  default     = true
  description = "Enables EBS encryption on the swap volume."
}

variable "volume_swap_throughput" {
  type        = number
  default     = null
  description = "Throughput to provision for the swap volume in mebibytes per second (MiB/s). This is only valid if 'volume_swap_type' is 'gp3'."
}

variable "volume_swap_force_detach" {
  type        = bool
  default     = true
  description = "Set to 'true' if you want to force the swap volume to detach. Useful if previous attempts failed, but use this option only as a last resort, as this can result in data loss."
}

variable "volume_swap_skip_destroy" {
  type        = bool
  default     = false
  description = "Set to 'true' if you do not wish to detach the swap volume from the instance to which it is attached at destroy time, and instead just remove the attachment from Terraform state."
}

################################################################################
# Locals
################################################################################

locals {
  volume_swap_iops       = contains(["gp3", "io2", "io1"], var.volume_swap_type) ? max(var.volume_swap_iops, var.volume_swap_type == "gp3" ? 3000 : 100) : null
  volume_swap_throughput = var.volume_swap_type == "gp3" ? var.volume_swap_throughput : null
}

################################################################################
# Resources
################################################################################

resource "aws_ebs_volume" "swap" {
  iops              = local.volume_swap_iops
  size              = var.volume_swap_size
  type              = var.volume_swap_type
  encrypted         = var.volume_swap_encrypted
  throughput        = local.volume_swap_throughput
  availability_zone = data.aws_subnet.scope.availability_zone

  tags = {
    VPC      = var.vpc
    Name     = format("%s.%s", var.class, local.name_suffix)
    Zone     = replace(data.aws_subnet.scope.availability_zone, data.aws_region.scope.name, "")
    Class    = var.class
    Owner    = var.owner
    Region   = data.aws_region.scope.name
    Company  = var.company
    Platform = var.platform
  }
}

################################################################################

resource "aws_volume_attachment" "swap" {
  volume_id    = aws_ebs_volume.swap.id
  device_name  = var.volume_swap_device
  instance_id  = aws_instance.scope.id
  force_detach = var.volume_swap_force_detach
  skip_destroy = var.volume_swap_skip_destroy
}

################################################################################
# Outputs
################################################################################

output "volume_swap_iops" {
  value = local.volume_swap_iops
}

output "volume_swap_size" {
  value = var.volume_swap_size
}

output "volume_swap_type" {
  value = var.volume_swap_type
}

output "volume_swap_device" {
  value = var.volume_swap_device
}

output "volume_swap_encrypted" {
  value = var.volume_swap_encrypted
}

output "volume_swap_throughput" {
  value = local.volume_swap_throughput
}

output "volume_swap_force_detach" {
  value = var.volume_swap_force_detach
}

output "volume_swap_skip_destroy" {
  value = var.volume_swap_skip_destroy
}

################################################################################

output "volume_swap_id" {
  value = aws_ebs_volume.swap.id
}

output "volume_swap_name" {
  value = aws_ebs_volume.swap.tags.Name
}

################################################################################

output "volume_swap_attachment_id" {
  value = aws_volume_attachment.swap.id
}

################################################################################
