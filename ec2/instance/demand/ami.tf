################################################################################
# Optional Variables
################################################################################

variable "ami_id" {
  type        = string
  default     = "*"
  description = "The AMI ID to use for the instance. Use '*' to lookup using the variables below instead."
}

variable "ami_name" {
  type        = string
  default     = "amzn2-ami-*-gp2"
  description = "Used to lookup the AMI to use for this instance. AMI name, can include wildcards."
}

variable "ami_owner" {
  type        = string
  default     = "137112412989"
  description = "Used to lookup the AMI to use for this instance. AWS account ID of the AMI owner."
}

variable "ami_state" {
  type        = string
  default     = "available"
  description = "Used to lookup the AMI to use for this instance. AMI state."
}

variable "ami_tag_key" {
  type        = string
  default     = "*"
  description = "Used to lookup the AMI to use for this instance. A tag key to lookup on, use with 'ami_tag_value', '*' to disable."
}

variable "ami_tag_value" {
  type        = string
  default     = "*"
  description = "Used to lookup the AMI to use for this instance. A tag value to lookup on, use with 'ami_tag_key', '*' to disable."
}

variable "ami_hypervisor" {
  type        = string
  default     = "hvm"
  description = "Used to lookup the AMI to use for this instance. AMI hypervisor."
}

variable "ami_image_type" {
  type        = string
  default     = "machine"
  description = "Used to lookup the AMI to use for this instance. AMI image type, should always be 'machine'."
}

variable "ami_most_recent" {
  type        = bool
  default     = true
  description = "Used to lookup the AMI to use for this instance. If multiple results are found, should the most recent be automatically used."
}

variable "ami_architecture" {
  type        = string
  default     = "x86_64"
  description = "Used to lookup the AMI to use for this instance. AMI architecture."
}

variable "ami_root_device_type" {
  type        = string
  default     = "ebs"
  description = "Used to lookup the AMI to use for this instance. AMI root device type."
}

################################################################################
# Data Sources
################################################################################

data "aws_ami" "scope" {
  owners      = tolist([var.ami_owner])
  most_recent = var.ami_most_recent

  filter {
    name   = "image-id"
    values = tolist([var.ami_id])
  }

  filter {
    name   = var.ami_id == "*" && var.ami_tag_key != "*" ? "tag-key" : "image-id"
    values = tolist([var.ami_id == "*" && var.ami_tag_key != "*" ? var.ami_tag_key : var.ami_id])
  }

  filter {
    name   = var.ami_id == "*" && var.ami_tag_value != "*" ? "tag-value" : "image-id"
    values = tolist([var.ami_id == "*" && var.ami_tag_value != "*" ? var.ami_tag_value : var.ami_id])
  }

  filter {
    name   = "name"
    values = tolist([var.ami_id == "*" && var.ami_tag_key == "*" && var.ami_tag_value == "*" ? var.ami_name : "*"])
  }

  filter {
    name   = "state"
    values = tolist([var.ami_id == "*" && var.ami_tag_key == "*" && var.ami_tag_value == "*" ? var.ami_state : "*"])
  }

  filter {
    name   = "image-type"
    values = tolist([var.ami_id == "*" && var.ami_tag_key == "*" && var.ami_tag_value == "*" ? var.ami_image_type : "*"])
  }

  filter {
    name   = "virtualization-type"
    values = tolist([var.ami_id == "*" && var.ami_tag_key == "*" && var.ami_tag_value == "*" ? var.ami_hypervisor : "*"])
  }

  filter {
    name   = "architecture"
    values = tolist([var.ami_id == "*" && var.ami_tag_key == "*" && var.ami_tag_value == "*" ? var.ami_architecture : "*"])
  }

  filter {
    name   = "root-device-type"
    values = tolist([var.ami_id == "*" && var.ami_tag_key == "*" && var.ami_tag_value == "*" ? var.ami_root_device_type : "*"])
  }
}

################################################################################
# Outputs
################################################################################

output "ami_tag_key" {
  value = var.ami_tag_key
}

output "ami_tag_value" {
  value = var.ami_tag_value
}

################################################################################

output "ami_id" {
  value = data.aws_ami.scope.id
}

output "ami_name" {
  value = data.aws_ami.scope.name
}

output "ami_tags" {
  value = data.aws_ami.scope.tags
}

output "ami_owner" {
  value = data.aws_ami.scope.owner_id
}

output "ami_state" {
  value = data.aws_ami.scope.state
}

output "ami_hypervisor" {
  value = data.aws_ami.scope.hypervisor
}

output "ami_image_type" {
  value = data.aws_ami.scope.image_type
}

output "ami_description" {
  value = data.aws_ami.scope.description
}

output "ami_most_recent" {
  value = data.aws_ami.scope.most_recent
}

output "ami_architecture" {
  value = data.aws_ami.scope.architecture
}

output "ami_image_location" {
  value = data.aws_ami.scope.image_location
}

output "ami_root_device_type" {
  value = data.aws_ami.scope.root_device_type
}

################################################################################
