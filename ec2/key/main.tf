################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The name of the SSH key to create."
}

################################################################################
# Optional Variables
################################################################################

variable "key" {
  type        = string
  default     = null
  description = "Raw content of SSH public key to upload in OpenSSH format."
}

variable "file" {
  type        = string
  default     = "~/.ssh/id_ed25519.pub"
  description = "Full path to the file that contains the public SSH key to upload. Ignored if 'key' is specified."
}

################################################################################
# Locals
################################################################################

locals {
  key  = var.key == null ? file(var.file) : var.key
  file = var.key == null ? var.file : null
}

################################################################################
# Resources
################################################################################

resource "aws_key_pair" "scope" {
  key_name   = var.name
  public_key = local.key
}

################################################################################
# Outputs
################################################################################

output "key" {
  value     = local.key
  sensitive = true
}

output "file" {
  value = local.file
}

################################################################################

output "name" {
  value = aws_key_pair.scope.key_name
}

################################################################################
