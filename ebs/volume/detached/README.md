<!----------------------------------------------------------------------------->

# ebs/volume/detached

#### Manage stand-alone [EBS] volumes

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/compute/aws//ebs/volume/detached`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_ebs_detached_volume" {
  source  = "gitlab.com/bitservices/compute/aws//ebs/volume/detached"
  name    = "foobar"
  size    = 5
  zone    = "a"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->

[EBS]: https://aws.amazon.com/ebs/

<!----------------------------------------------------------------------------->
