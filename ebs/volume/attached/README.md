<!----------------------------------------------------------------------------->

# ebs/volume/attached

#### Manage [EBS] volumes that are attached to [EC2] instances

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/compute/aws//ebs/volume/attached`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_ec2_key" {
  source = "gitlab.com/bitservices/compute/aws//ec2/key"
  name   = "my-key"
}

module "my_ec2_demand_instance" {
  source  = "gitlab.com/bitservices/compute/aws//ec2/instance/demand"
  key     =  module.my_ec2_key.name
  zone    = "b"
  class   = "foo.bar"
  owner   = var.owner
  company = var.company
  vpc     = "sandpit01"
}

module "my_ebs_attached_volume" {
  source                 = "gitlab.com/bitservices/compute/aws//ebs/volume/attached"
  name                   = format("swap-%s", module.my_ec2_demand_instance.name)
  size                   = 5
  zone                   = module.my_ec2_demand_instance.subnet_zone
  owner                  = var.owner
  company                = var.company
  attachment_instance_id = module.my_ec2_demand_instance.id
}
```

<!----------------------------------------------------------------------------->

[EBS]: https://aws.amazon.com/ebs/
[EC2]: https://aws.amazon.com/ec2/

<!----------------------------------------------------------------------------->
