################################################################################
# Required Variables
################################################################################

variable "attachment_instance_id" {
  type        = string
  description = "An EC2 instance ID to attach the EBS volume to."
}

################################################################################
# Optional Variables
################################################################################

variable "attachment_device" {
  type        = string
  default     = "/dev/sdf"
  description = "The device name to expose to the EC2 instances."
}

variable "attachment_force_detach" {
  type        = bool
  default     = false
  description = "Set to 'true' if you want to force the EBS volume to detach. Useful if previous attempts failed, but use this option only as a last resort, as this can result in data loss."
}

variable "attachment_skip_destroy" {
  type        = bool
  default     = false
  description = "Set to 'true' if you do not wish to detach the EBS volume from the instance to which it is attached at destroy time, and instead just remove the attachment from Terraform state."
}

################################################################################
# Resources
################################################################################

resource "aws_volume_attachment" "scope" {
  volume_id    = aws_ebs_volume.scope.id
  device_name  = var.attachment_device
  instance_id  = var.attachment_instance_id
  force_detach = var.attachment_force_detach
  skip_destroy = var.attachment_skip_destroy
}

################################################################################
# Outputs
################################################################################

output "attachment_device" {
  value = var.attachment_device
}

output "attachment_force_detach" {
  value = var.attachment_force_detach
}

output "attachment_skip_destroy" {
  value = var.attachment_skip_destroy
}

################################################################################

output "attachment_id" {
  value = aws_volume_attachment.scope.id
}

output "attachment_volume_id" {
  value = aws_volume_attachment.scope.volume_id
}

output "attachment_instance_id" {
  value = aws_volume_attachment.scope.instance_id
}

################################################################################
