################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The full name of the EBS volume."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################

variable "size" {
  type        = number
  description = "The size of the drives in GiBs."
}

variable "zone" {
  type        = string
  description = "Which availbility zone should this EBS volume be placed. Specified as a single letter."
}

################################################################################
# Optional Variables
################################################################################

variable "iops" {
  type        = number
  default     = 0
  description = "The amount of IOPS to provision for the disk. This is only valid if 'type' is 'gp3', 'io2' or 'io1'."
}

variable "type" {
  type        = string
  default     = "standard"
  description = "The type of EBS volume. Can be 'standard', 'gp3', 'gp2', 'io2', 'io1', 'st1' or 'sc1'."

  validation {
    condition     = contains(["standard", "gp3", "gp2", "io2", "io1", "st1", "sc1"], var.type)
    error_message = "The EBS volume must be of type: 'standard', 'gp3', 'gp2', 'io2', 'io1', 'st1' or 'sc1'."
  }
}

variable "encrypted" {
  type        = bool
  default     = true
  description = "If 'true', the disk will be encrypted."
}

variable "kms_key_id" {
  type        = string
  default     = null
  description = "The ARN for the KMS encryption key. 'encrypted' must be 'true' to specify this option."
}

variable "restore_id" {
  type        = string
  default     = null
  description = "A snapshot to base the EBS volume from."
}

variable "throughput" {
  type        = number
  default     = null
  description = "Throughput to provision for the EBS volume in mebibytes per second (MiB/s). This is only valid if 'type' is 'gp3'."
}

################################################################################
# Locals
################################################################################

locals {
  iops       = contains(["gp3", "io2", "io1"], var.type) ? max(var.iops, var.type == "gp3" ? 3000 : 100) : null
  kms_key_id = var.encrypted ? var.kms_key_id : null
  throughput = var.type == "gp3" ? var.throughput : null
}

################################################################################
# Resources
################################################################################

resource "aws_ebs_volume" "scope" {
  iops              = local.iops
  size              = var.size
  type              = var.type
  encrypted         = var.encrypted
  kms_key_id        = local.kms_key_id
  throughput        = local.throughput
  snapshot_id       = var.restore_id
  availability_zone = format("%s%s", data.aws_region.scope.name, var.zone)

  tags = {
    Name    = var.name
    Zone    = var.zone
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "size" {
  value = var.size
}

output "zone" {
  value = var.zone
}

################################################################################

output "iops" {
  value = local.iops
}

output "type" {
  value = var.type
}

output "encrypted" {
  value = var.encrypted
}

output "kms_key_id" {
  value = local.kms_key_id
}

output "throughput" {
  value = local.throughput
}

################################################################################

output "id" {
  value = aws_ebs_volume.scope.id
}

output "name" {
  value = aws_ebs_volume.scope.tags.Name
}

output "restore_id" {
  value = aws_ebs_volume.scope.snapshot_id
}

################################################################################
